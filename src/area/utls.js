// 导出 对应的不同接口的URL地址
module.exports = {
  domain: {
    api1: 'http://localhost:8765', // 模块一接口地址
    api2: 'http://10.1.20.14:8779' // 模块二接口地址
    // Base_M3_URL: 'http://192.168.2.102:6608', // 模块三接口地址
    // Base_M4_URL: 'http://192.168.2.222:9000' // 模块四接口地址
  }
};
