const required = true;
const trigger = ['blur', 'change'];

export const formValidate = {
  // 文本框必填校验
  textRequired(maxlength = 64, message = '请填写') {
    return {
      message,
      required,
      trigger
    };
  },

  // 必须为数字得校验
  isNumberRule(message = '数字需为0以上', min = 0) {
    return {
      type: 'number',
      message,
      min,
      trigger
    };
  },

  // 必须为数字得校验
  eMail(message = '请输入正确的邮箱格式') {
    return {
      pattern: /^[A-Za-z0-9]+([_\\.][A-Za-z0-9]+)*@([A-Za-z0-9\\-]+\.)+[A-Za-z]{2,6}$/,
      message,
      trigger
    };
  },

  // 身份证号
  identityNumber(message = '请输入正确的身份证号格式') {
    return {
      pattern: /(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$)/,
      message,
      trigger
    };
  },

  // 手机号
  phone(message = '请输入正确的手机号码格式') {
    return {
      pattern: /^1[3456789]\d{9}$/,
      message,
      trigger
    };
  }
};
