/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const dictRouter = {
  path: '/dict',
  component: Layout,
  redirect: '/dict/index',
  name: 'Index',
  meta: {
    title: '字典管理',
    icon: 'distribution'
  },
  children: [
    {
      path: 'index',
      component: () => import('@/components/dict/Index'),
      name: 'Index',
      meta: { title: '字典管理' }
    },
    {
      path: 'type/data/:code',
      component: () => import('@/components/dict/Data'),
      name: 'Data',
      meta: { title: '字典数据' },
      hidden: true
    }
  ]
};

export default dictRouter;
