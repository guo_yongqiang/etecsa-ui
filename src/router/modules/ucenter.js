/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const ucenterRouter = {
  path: '/ucenter',
  component: Layout,
  redirect: '/ucenter/user',
  name: 'ucenter',
  meta: {
    title: '用户中心',
    icon: 'ucenter'
  },
  children: [
    {
      path: 'user',
      component: () => import('@/components/ucenter/User'),
      name: 'User',
      meta: { title: '用户列表' }
    },
    {
      path: 'role',
      component: () => import('@/components/ucenter/Role'),
      name: 'Role',
      meta: { title: '角色列表' }
    },
    {
      path: 'permissions',
      component: () => import('@/components/ucenter/Permissions'),
      name: 'Permissions',
      meta: { title: '权限管理' }
    },
    {
      path: 'route',
      component: () => import('@/components/ucenter/Route'),
      name: 'Route',
      meta: { title: '路由列表' }
    }
  ]
};
export default ucenterRouter;
