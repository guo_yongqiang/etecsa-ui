/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const docRouter = {
  path: '/doc',
  component: Layout,
  redirect: '/doc/catalog',
  name: 'Doc',
  meta: {
    title: '文档管理',
    icon: 'doc'
  },
  children: [
    {
      path: 'catalog',
      component: () => import('@/components/doc/Catalog'),
      name: 'Catalog',
      meta: { title: '接口目录管理' }
    },
    {
      path: 'api-response-page',
      component: () => import('@/components/doc/ApiResponsePage'),
      name: 'ApiResponsePage',
      meta: { title: '响应码管理' }
    },
    {
      path: 'api-history-page',
      component: () => import('@/components/doc/ApiHistoryPage'),
      name: 'ApiHistoryPage',
      meta: { title: '接口变更管理' }
    },
    {
      path: 'api-page',
      component: () => import('@/components/doc/ApiPage'),
      name: 'ApiPage',
      meta: { title: '接口管理' }
    },
    {
      path: 'api-create',
      component: () => import('@/components/doc/ApiCreate'),
      name: 'ApiCreate',
      meta: { title: '新增接口' },
      hidden: true
    },
    {
      path: 'api-update/:id',
      component: () => import('@/components/doc/ApiUpdate'),
      name: 'ApiUpdate',
      meta: { title: '编辑接口' },
      hidden: true
    },
    {
      path: 'api-detail/:id',
      component: () => import('@/components/doc/ApiDetail'),
      name: 'ApiDetail',
      meta: { title: '接口详情' },
      hidden: true
    }
  ]
};
export default docRouter;
