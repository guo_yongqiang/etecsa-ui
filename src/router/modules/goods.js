/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const goodsRouter = {
  path: '/goods',
  component: Layout,
  redirect: '/goods/goods',
  name: 'goods',
  meta: {
    title: '商品管理',
    icon: 'goods'
  },
  children: [
    {
      path: 'goods',
      component: () => import('@/components/goods/GoodsList'),
      name: 'goods',
      meta: { title: '商品列表' }
    },
    {
      path: 'goodsadd',
      component: () => import('@/components/goods/AddGoods'),
      name: 'goodsadd',
      meta: { title: '商品新增' }
    },
    {
      path: 'goodsedit',
      component: () => import('@/components/goods/UpdateGoods'),
      name: 'UpdateGoods',
      meta: { title: '商品修改' },
      hidden: true
    },
    {
      path: 'brand',
      component: () => import('@/components/goods/GoodsBrand'),
      name: 'brand',
      meta: { title: '品牌列表' }
    },
    {
      path: 'goodsCategory',
      component: () => import('@/components/goods/GoodsCategories'),
      name: 'goodsCategory',
      meta: { title: '商品分类' }
    },
    {
      path: 'specifications',
      component: () => import('@/components/goods/GoodsSpecifications'),
      name: 'specifications',
      meta: { title: '商品规格' }
    },
    {
      path: 'carousel',
      component: () => import('@/components/goods/Carousel'),
      name: 'carousel',
      meta: { title: '商品轮播图' }
    },
    {
      path: 'category',
      component: () => import('@/components/goods/IndexCategory'),
      name: 'category',
      meta: { title: '商品首页分类' }
      // children: [
      //   {
      //     path: 'categoryDetail/:id',
      //     component: () => import('@/components/goods/CategoryDetail'),
      //     name: 'CategoryDetail',
      //     meta: { title: '商品详情页' }
      //   }
      // ]
    },
    {
      path: 'categoryDetail/:id',
      component: () => import('@/components/goods/CategoryDetail'),
      name: 'CategoryDetail',
      meta: { title: '商品详情页' },
      hidden: true
    }
  ]
};
export default goodsRouter;
