/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const distributionRouter = {
  path: '/distribution',
  component: Layout,
  redirect: '/distribution/distributor',
  name: 'Distribution',
  meta: {
    title: '分销管理',
    icon: 'distribution'
  },
  children: [
    {
      path: 'distributor',
      component: () => import('@/components/distribution/DistributorPage'),
      name: 'Distributor',
      meta: { title: '分销商管理' }
    },
    {
      path: 'distributor/:id',
      component: () => import('@/components/distribution/DistributorDetail'),
      name: 'DistributorDetail',
      meta: { title: '分销商详情' },
      hidden: true
    },
    {
      path: 'distributor-level',
      component: () => import('@/components/distribution/DistributorLevel'),
      name: 'DistributorLevel',
      meta: { title: '分销商级别管理' }
    },
    {
      path: 'distributor-member',
      component: () => import('@/components/distribution/MemberPage'),
      name: 'Member',
      meta: { title: '分销会员管理' }
    },
    {
      path: 'member-profit-param',
      component: () => import('@/components/distribution/MemberProfitParam'),
      name: 'MemberProfitParam',
      meta: { title: '会员分润规则设置' }
    },
    {
      path: 'profit-record',
      component: () => import('@/components/distribution/ProfitRecordPage'),
      name: 'ProfitRecord',
      meta: { title: '分润信息' }
    },
    {
      path: 'invite-cash-back-page',
      component: () => import('@/components/distribution/InviteCashBackPage'),
      name: 'InviteCashBackPage',
      meta: { title: '邀请返利' }
    },
    {
      path: 'withdraw-param',
      component: () => import('@/components/distribution/withdrawParam'),
      name: 'WithdrawParam',
      meta: { title: '提现费率设置' }
    },
    {
      path: 'withdraw-record-page',
      component: () => import('@/components/distribution/withdrawRecordPage'),
      name: 'WithdrawRecordPage',
      meta: { title: '提现记录' }
    },
    {
      path: 'amount-page',
      component: () => import('@/components/distribution/AmountPage'),
      name: 'AmountPage',
      meta: { title: '账单管理' }
    },
    {
      path: 'trade-dynamics',
      component: () => import('@/components/distribution/TradeDynamics'),
      name: 'TradeDynamics',
      meta: { title: '交易动态' }
    }
  ]
};
export default distributionRouter;
