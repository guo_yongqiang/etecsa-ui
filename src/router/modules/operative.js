/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const operativeRouter = {
  path: '/operative',
  component: Layout,
  redirect: '/operative/advert',
  name: 'operative',
  meta: {
    title: '运营管理',
    icon: 'operative'
  },
  children: [
    {
      path: 'advert',
      component: () => import('@/components/operative/Advert'),
      name: 'Advert',
      meta: { title: '广告列表' }
    },
    {
      path: 'seckill',
      component: () => import('@/components/operative/Seckill'),
      name: 'Seckill',
      meta: { title: '活动秒杀' }
    },
    {
      path: 'brandRec',
      component: () => import('@/components/operative/BrandRec'),
      name: 'BrandRec',
      meta: { title: '品牌推荐' }
    },
    {
      path: 'buzzRec',
      component: () => import('@/components/operative/BuzzRec'),
      name: 'BuzzRec',
      meta: { title: '人气推荐' }
    },
    {
      path: 'coupon',
      component: () => import('@/components/operative/Coupon'),
      name: 'Coupon',
      meta: { title: '优惠券' }
    },
    {
      path: 'couponDetail',
      component: () => import('@/components/operative/CouponDetail'),
      name: 'CouponDetail',
      meta: { title: '优惠券详情' }
    },
    {
      path: 'newProduct',
      component: () => import('@/components/operative/NewGoodsList'),
      name: 'NewProduct',
      meta: { title: '新品推荐' }
    },
    {
      path: 'specialSub',
      component: () => import('@/components/operative/SpecialSub'),
      name: 'SpecialSub',
      meta: { title: '专题推荐' }
    },
    {
      path: 'productHome',
      component: () => import('@/components/operative/ProductHome'),
      name: 'productHome',
      meta: { title: '首页推荐' }
    },
    {
      path: 'productHomeGoods',
      component: () => import('@/components/operative/ProductHomeGoods'),
      name: 'productHomeGoods',
      meta: { title: '首页推荐商品' }
    }
  ]
};
export default operativeRouter;
