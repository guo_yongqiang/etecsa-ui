import Layout from '@/components/Home';

const fileRouter = {
  path: '/file',
  component: Layout,
  redirect: '/file/fileupload',
  name: 'File',
  meta: {
    title: '文件管理',
    icon: 'file'
  },
  children: [
    {
      path: 'fileupload',
      component: () => import('@/components/file/FileUpload'),
      name: 'FileUpload',
      meta: { title: '文件上传' }
    }
  ]
};
export default fileRouter;
