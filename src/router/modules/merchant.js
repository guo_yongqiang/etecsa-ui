/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const payRouter = {
  path: '/merchant',
  component: Layout,
  redirect: '/merchant/tenant',
  name: 'Merchant',
  meta: {
    title: '商户管理',
    icon: 'merchant'
  },
  children: [
    {
      path: 'tenant',
      component: () => import('@/components/merchant/Tenant'),
      name: 'Tenant',
      meta: { title: '全部商户' }
    },
    {
      path: 'termin',
      component: () => import('@/components/merchant/Termin'),
      name: 'Termin',
      meta: { title: '终端查询' }
    },
    {
      path: 'editTenant/:id',
      component: () => import('@/components/merchant/EditTenant'),
      name: 'EditTenant',
      meta: { title: '终端查询' },
      hidden: true
    },
    {
      path: 'data',
      component: () => import('@/components/merchant/Data'),
      name: 'Data',
      meta: { title: '数据统计' }
    },
    {
      path: 'add-tenant',
      component: () => import('@/components/merchant/AddTenant'),
      name: 'Addtenant',
      meta: { title: '添加商户' }
    },
    {
      path: 'goods-ment',
      component: () => import('@/components/merchant/GoodsMent'),
      name: 'Goodsment',
      meta: { title: '商品券管理' }
    },
    {
      path: 'liquid-enquiries',
      component: () => import('@/components/merchant/LiquidEnquiries'),
      name: 'LiquidEnquiries',
      meta: { title: '清算查询' }
    },
    {
      path: 'audit-records',
      component: () => import('@/components/merchant/AuditRecords'),
      name: 'Auditrecords',
      meta: { title: '审核记录' }
    },
    {
      path: 'cash-record',
      component: () => import('@/components/merchant/CashRecord'),
      name: 'CashRecord',
      meta: { title: '提现记录' }
    },
    {
      path: 'refut-record',
      component: () => import('@/components/merchant/RefutRecord'),
      name: 'RefutRecord',
      meta: { title: '审核驳回' }
    },
    {
      path: 'transaction',
      component: () => import('@/components/merchant/Transaction'),
      name: 'Transaction',
      meta: { title: '交易统计' }
    },
    {
      path: 'drawback',
      component: () => import('@/components/merchant/Drawback'),
      name: 'Drawback',
      meta: { title: '交易退款' }
    }
  ]
};
export default payRouter;
