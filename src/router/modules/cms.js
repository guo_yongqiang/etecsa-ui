/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const cmsRouter = {
  path: '/cms',
  component: Layout,
  redirect: '/cms/article',
  name: 'Cms',
  meta: {
    title: '内容管理',
    icon: 'cms'
  },
  children: [
    {
      path: 'article',
      component: () => import('@/components/cms/Article'),
      name: 'Article',
      meta: { title: '文章管理' }
    },
    {
      path: 'carousel',
      component: () => import('@/components/cms/Carousel'),
      name: 'Carousel',
      meta: { title: '轮播图管理' }
    },
    {
      path: 'catalogNotice',
      component: () => import('@/components/cms/CatalogNotice'),
      name: 'CatalogNotice',
      meta: { title: '栏目公告管理' }
    }
  ]
};
export default cmsRouter;
