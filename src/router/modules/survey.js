/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const surveyRouter = {
  path: '/survey',
  component: Layout,
  redirect: '/survey/paper',
  name: 'survey',
  meta: {
    title: '问卷调查',
    icon: 'survey'
  },
  children: [
    {
      path: 'paper',
      component: () => import('@/components/survey/Paper'),
      name: 'Paper',
      meta: { title: '问卷调查' }
    },
    {
      path: 'addpaper',
      component: () => import('@/components/survey/AddPaper'),
      name: 'AddPaper',
      meta: { title: '添加问卷调查' }
    }
  ]
};
export default surveyRouter;
