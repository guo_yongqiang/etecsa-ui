/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const memberRouter = {
  path: '/member',
  component: Layout,
  redirect: '/member/Member',
  name: 'member',
  meta: {
    title: '会员管理',
    icon: 'member'
  },
  children: [
    {
      path: 'member',
      component: () => import('@/components/member/Member'),
      name: 'member',
      meta: { title: '会员列表' }
    },
    {
      path: 'memberAddress',
      component: () => import('@/components/member/MemberAddress'),
      name: 'memberAddress',
      meta: { title: '收货地址管理' }
    },
    {
      path: 'memberCollect',
      component: () => import('@/components/member/MemberCollect'),
      name: 'memberCollect',
      meta: { title: '会员收藏' }
    },
    {
      path: 'memberTask',
      component: () => import('@/components/member/MemberTask'),
      name: 'memberTask',
      meta: { title: '会员任务' }
    }
  ]
};
export default memberRouter;
