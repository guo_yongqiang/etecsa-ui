/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const systemRouter = {
  path: '/doc',
  component: Layout,
  redirect: '/doc/catalog',
  name: 'Doc',
  meta: {
    title: '文档管理',
    icon: 'doc'
  },
  children: [
    {
      path: 'catalog',
      component: () => import('@/components/doc/Catalog'),
      name: 'Catalog',
      meta: { title: '接口目录管理' }
    },
    {
      path: 'res-code',
      component: () => import('@/components/doc/ApiResponsePage'),
      name: 'ResCode',
      meta: { title: '响应码管理' }
    },
    {
      path: 'api-history',
      component: () => import('@/components/doc/ApiHistoryPage'),
      name: 'ApiHistory',
      meta: { title: '接口变更管理' }
    },
    {
      path: 'api-page',
      component: () => import('@/components/doc/ApiPage'),
      name: 'Api-Page',
      meta: { title: '接口管理' }
    },
    {
      path: 'api-create',
      component: () => import('@/components/doc/ApiCreate'),
      name: 'ApiCreate',
      meta: { title: '添加接口' }
    },
    {
      path: 'api-update',
      component: () => import('@/components/doc/ApiUpdate'),
      name: 'ApiUpdate',
      meta: { title: '编辑接口' }
    },
    {
      path: 'api-detail',
      component: () => import('@/components/doc/ApiDetail'),
      name: 'ApiDetail',
      meta: { title: '接口详情' }
    }
  ]
};
export default systemRouter;
