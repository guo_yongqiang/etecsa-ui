/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/components/Home';

const orderRouter = {
  path: '/order',
  component: Layout,
  redirect: '/order/orders',
  name: 'Order',
  meta: {
    title: '订单管理',
    icon: 'order'
  },
  children: [
    {
      path: 'orders',
      component: () => import('@/components/order/Orders'),
      name: 'Catalog',
      meta: { title: '订单列表' }
    },
    {
      path: 'retOrder',
      component: () => import('@/components/order/RetOrder'),
      name: 'RetOrder',
      meta: { title: '退货管理' }
    },
    {
      path: 'viewsPage/:id',
      component: () => import('@/components/order/ViewsPage'),
      name: 'ViewsPage',
      meta: { title: '退货订单详情' },
      hidden: true
    }
  ]
};
export default orderRouter;
