import Vue from 'vue';
import Router from 'vue-router';

/* Layout */
import Layout from '@/components/Home';

import docRouter from '@/router/modules/doc';
import distributionRouter from '@/router/modules/distribution';
import memberRouter from '@/router/modules/member';
import orderRouter from '@/router/modules/order';
import merchantRouter from '@/router/modules/merchant';
import operativeRouter from '@/router/modules/operative';
import ucenterRouter from '@/router/modules/ucenter';
import goodsRouter from '@/router/modules/goods';
import cmsRouter from '@/router/modules/cms';
import survey from '@/router/modules/survey';
import fileRouter from '@/router/modules/file';
import dictRouter from '@/router/modules/dict';
Vue.use(Router);

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/home',
    component: () => import('@/components/Home'),
    hidden: true
  },
  {
    path: '/login',
    component: () => import('@/components/Login'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/components/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/welcome',
    children: [{
      path: '/welcome',
      name: 'Welcome',
      component: () => import('@/components/Welcome'),
      meta: { title: '首页', icon: 'welcome' }
    }]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
];

export const asyncRouters = [
  distributionRouter,
  docRouter,
  memberRouter,
  orderRouter,
  merchantRouter,
  ucenterRouter,
  goodsRouter,
  cmsRouter,
  operativeRouter,
  survey,
  fileRouter,
  dictRouter
];

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
});

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
