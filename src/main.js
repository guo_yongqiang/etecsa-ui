import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import App from './App.vue';
import store from './store';
import router from './router';

import './icons'; // icon

import tinymce from 'tinymce/tinymce';

import '@/permission';

// import Router from 'vue-router';
import TreeTable from 'vue-table-with-tree-grid';
// import store from './store.js';

import { FormatDate } from '@/filters';

// 富文本编辑器
import VueQuillEditor from 'vue-quill-editor';

import axios from 'axios';

// 导入Nprogress
// import Nprogress from 'nprogress';
import 'nprogress/nprogress.css';

// require styles
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';

import './assets/css/global.css';

import 'xe-utils';
import VXETable from 'vxe-table';
import 'vxe-table/lib/index.css';

Vue.use(VXETable);

Vue.config.productionTip = false;

Vue.use(ElementUI);

Vue.use(tinymce);

Vue.use(VueQuillEditor);

// Vue.use(Store);
Vue.prototype.$axios = axios;
Vue.component('tree-table', TreeTable);

// 定义一个时间过滤器
Vue.filter('FormatDate', function(date, fmt) {
  return FormatDate(date, fmt);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
