const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  sftpServe: state => state.common.sftpServe,
  addRouters: state => state.permission.addRouters,
  routers: state => state.permission.routers,
  moduleDict: state => state.dict.MODULE,
  fileTypeDict: state => state.dict.FILE_TYPE
};
export default getters;
