import { listByParentCode } from '@/api/system/system-dict-detail';

const state = {
  FILE_TYPE: [],
  MODULE: []
};

const mutations = {
  getDicData(state, data) {
    state[data.dicCodes] = data.res;
  }
};

const actions = {
  getDicData(context, dicCodes) {
    for (let i = 0; i < dicCodes.length; i++) {
      if (state[dicCodes[i]].length === 0) {
        listByParentCode(dicCodes[i]).then(res => {
          res = res.data;
          context.commit('getDicData', { dicCodes: dicCodes[i], res: res });
        });
      }
    }
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
