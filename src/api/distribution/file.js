import request from '@/utils/request';

const DOC = {
  MODULE: 'distribution',
  MAPPING: 'files'
};

export const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

export const URL = {
  upload: `${PATH}/upload`,
  batchUpload: `${PATH}/batch-upload`
};

/**
 * 创建分销商
 *
 * @returns {Promise}
 */
export function upload() {
  return request.post(URL.upload);
}

/**
 * 批量上传
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function batchUpload() {
  return request.post(URL.batchUpload);
}
