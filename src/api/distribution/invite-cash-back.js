import request from '@/utils/request';
import { param } from '@/utils';

const DOC = {
  MODULE: 'distribution',
  MAPPING: 'invite-cash-backs'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * （批量）删除邀请返利表信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 根据Id查询邀请返利表详细信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询邀请返利表列表信息
 *
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数查询邀请返利表分页列表信息
 *
 * @returns {Promise}
 */
export function page(json) {
  return request.get(
    `${PATH}/pages?pageNo=${json.pageNo}&pageSize=${json.pageSize}&${param(json.params)}`
  );
}
