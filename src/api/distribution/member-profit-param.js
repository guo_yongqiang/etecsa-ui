import request from '@/utils/request';

const DOC = {
  MODULE: 'distribution',
  MAPPING: 'member-profit-params'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 创建会员分润参数设置信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * （批量）删除会员分润参数设置信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 更新会员分润参数设置信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 根据Id查询会员分润参数设置详细信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询会员分润参数设置列表信息
 *
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}
