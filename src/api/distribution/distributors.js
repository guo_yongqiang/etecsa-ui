import request from '@/utils/request';
import { param } from '@/utils';

const DOC = {
  MODULE: 'distribution',
  MAPPING: 'distributors'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 创建分销商
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 审核分销商信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function check(id, data) {
  return request.put(
    `${PATH}/${id}/approve`,
    data
  );
}

/**
 * 缴费完成
 *
 * @param id
 * @returns {Promise}
 */
export function payCompleted(id) {
  return request.get(
    `${PATH}/${id}/pay-completed`
  );
}

/**
 * （批量）删除分销商信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 更新分销商信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function updateById(id, data) {
  return request.put(
    `${PATH}/${id}`, data
  );
}

/**
 * 更新密码
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function updatePwd(id, data) {
  return request.put(
    `${PATH}/${id}/password`, data
  );
}

/**
 * 获取分销商详细信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 获取分销商直属下级
 *
 * @param id
 * @returns {Promise}
 */
export function subordinateMember(id) {
  return request.get(
    `${PATH}/${id}/subordinate`
  );
}

/**
 * 获取分销商直属下级
 *
 * @param id
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 获取分销商直属下级
 *
 * @returns {Promise}
 */
export function page(json) {
  return request.get(
    `${PATH}/pages?pageNo=${json.pageNo}&pageSize=${json.pageSize}&${param(json.params)}`
  );
}
