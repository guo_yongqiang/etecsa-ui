import request from '@/utils/request';
import { param } from '@/utils';

const DOC = {
  MODULE: 'distribution',
  MAPPING: 'distributor-levels'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 创建分销商级别信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 删除分销商级别信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 更新分销商级别信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 根据Id查询分销商级别详细信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询分销商级别列表信息
 *
 * @returns {Promise}
 */
export function list(json) {
  let url = json ? `${PATH}?${param(json.params)}` : `${PATH}`;
  return request.get(url);
}
