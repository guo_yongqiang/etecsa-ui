import request from '@/utils/request';

const DOC = {
  MODULE: 'distribution',
  MAPPING: 'api'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 根据交易记录进行分润
 *
 * @param data
 * @returns {Promise}
 */
export function tradeOrder(data) {
  return request.post(
    `${PATH}/trade-order`, data
  );
}

/**
 * 根据交易记录进行分润
 *
 * @param accountId
 * @returns {Promise}
 */
export function tradeDynamics(accountId) {
  return request.get(
    `${PATH}/trade-dynamics/${accountId}`
  );
}

/**
 * （批量）获取分润记录
 *
 * @param json
 * @returns {Promise}
 */
export function profitRecord(json) {
  return request.get(
    `${PATH}/profit-record?pageNo=${json.pageNo}&pageSize=${json.pageSize}`, json
  );
}
