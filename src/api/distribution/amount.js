import request from '@/utils/request';

const DOC = {
  MODULE: 'distribution',
  MAPPING: 'amounts'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * （批量）根据参数查询账户金额表分页列表信息
 *
 * @param json
 * @returns {Promise}
 */
export function page(json) {
  return request.get(
    `${PATH}/pages?pageNo=${json.pageNo}&pageSize=${json.pageSize}`, json
  );
}
