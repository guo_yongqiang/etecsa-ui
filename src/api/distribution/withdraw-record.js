import request from '@/utils/request';
import { param } from '@/utils';

const DOC = {
  MODULE: 'distribution',
  MAPPING: 'withdraw-records'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 申请提现
 *
 * @param data
 * @returns {Promise}
 */
export function applyWithdraw(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 处理提现
 *
 * @param id
 * @param withdrawStatus
 * @return {Promise}
 */
export function dealWithdraw(id, withdrawStatus) {
  return request.post(
    `${PATH}/${id}/deal-withdraw?withdrawStatus=${withdrawStatus}`
  );
}

/**
 * 删除提现表信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 根据Id查询分销会员表详细信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询分销会员表列表信息
 *
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数查询分销会员表分页列表信息
 *
 * @returns {Promise}
 */
export function page(json) {
  return request.get(
    `${PATH}/pages?pageNo=${json.pageNo}&pageSize=${json.pageSize}&${param(json.params)}`
  );
}
