import request from '@/utils/request';
import { param } from '@/utils';
import fileDownload from 'js-file-download';

const CMS = {
  MODULE: 'cms',
  MAPPING: 'cms-attachments'
};

const PATH = `${CMS.MODULE}/${CMS.MAPPING}`;

export const URL = {
  upload: `${PATH}/upload`,
  batchUpload: `${PATH}/batch-upload`
};

/**
 * 附件上传
 *
 * @param json
 * @returns {Promise}
 */
export function upload(json) {
  return request.post(
    URL.upload, json
  );
}

/**
 * 附件下载
 *
 * @param id
 * @param name
 * @returns {Promise}
 */
export function download(id, name) {
  return request.get(`${PATH}/${id}/download`, { responseType: 'arraybuffer' }).then(res => {
    fileDownload(res, name);
  });
}

/**
 * 根据Id查询附件表记录
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 删除
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 更新附件表记录
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function updateById(id, data) {
  return request.put(
    `${PATH}/${id}`, data
  );
}

/**
 * 描述：根据参数查询附件表分页记录
 *
 * @returns {Promise}
 */
export function page(json) {
  return request.get(
    `${PATH}/pages?pageNo=${json.pageNo}&pageSize=${json.pageSize}&${param(json.params)}`
  );
}
