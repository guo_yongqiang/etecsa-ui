import request from '@/utils/request';

const PRODUCT = {
    MODULE: 'order',
    MAPPING: 'flash-promotion-product-relations'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 创建限时购场次表
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
    return request.post(
        `${PATH}`, data
    );
}

/**
 * （批量）删除商品限时购与商品关系表 信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
    return request.detele(
        `${PATH}/${ids}`
    );
}

/**
 *  修改上下线状态-API接口信息
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
    return request.put(
        `${PATH}/update/status`, data
    );
}

/**
 * 根据Id查询订单详情-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
    return request.get(
        `${PATH}/${id}`
    );
}

/**
 * 更新限时购场次表-API接口信息
 *
 * @param id
 * @param status
 * @returns {Promise}
 */
export function updateStatus(id, status) {
    return request.post(
        `${PATH}/update/status/${id}`, status
    );
}

/**
 * 查询订单表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
    return request.get(
        `${PATH}`
    );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
    return request.get(
        `${PATH}/pages`, data
    );
}



