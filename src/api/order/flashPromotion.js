import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'order',
  MAPPING: 'flash-promotions'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 添加活动
 *
 * @param data
 * @returns {Promise}
 */
export function create(data) {
  return request.post(
    `${PATH}`, data
  );
}
/**
 * （批量）删除限时购表 信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.detele(
    `${PATH}/${ids}`
  );
}

/**
 *  修改上下线状态-API接口信息
 *@param status
 * @param id
 * @returns {Promise}
 */
export function update(id, status) {
  return request.post(
    `${PATH}/update/status/${id}`, status
  );
}

/**
 * 根据Id查询订单详情-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询订单表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}

/**
 * 秒杀
 *
 * @param data
 * @returns {Promise}
 */
export function spike(data) {
  return request.post(
    `${PATH}/spike`, data
  );
}

/**
 * 轮询接口 队列模式
 *
 * @param data
 * @returns {Promise}
 */
export function seckillPollingQueue(data) {
  return request.post(
    `${PATH}/seckill-polling-queue`, data
  );
}

/**
 * 秒杀下单接口
 *
 * @param orderId
 * @returns {Promise}
 */
export function createOrder(orderId) {
  return request.post(
    `${PATH}/createOrder`, orderId
  );
}

