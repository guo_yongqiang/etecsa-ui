import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'order',
  MAPPING: 'flash-promotion-product-relations'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 批量选择商品添加关联
 *
 * @param data
 * @returns {Promise}
 */
export function create(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 查看活动库存情况
 *
 * @param id
 * @returns {Promise}
 */
export function create(id) {
  return request.post(
    `${PATH}/stock-num`, id
  );
}

/**
 * （批量）删除商品限时购与商品关系表 信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.detele(
    `${PATH}/${ids}`
  );
}

/**
 *  修改上下线状态-API接口信息
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}/update/status`, data
  );
}

/**
 * 根据Id查询订单详情-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询订单表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}



