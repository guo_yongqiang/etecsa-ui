import request from '@/utils/request';

const PRODUCT = {
    MODULE: 'order',
    MAPPING: 'order-comment'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 添加评价
 *
 * @param data
 * @returns {Promise}
 */
export function saveComment(data) {
    return request.post(
        `${PATH}`, data
    );
}


/**
 * 根据userid查询评论
 *
 * @param id
 * @returns {Promise}
 */
export function getByuserId(id) {
    return request.get(
        `${PATH}/user/${id}`
    );
}

/**
 * 根据订单号查询评论
 *
 * @param sn
 * @returns {Promise}
 */
export function getByOrderSn(sn) {
    return request.detele(
        `${PATH}/order/${sn}`
    );
}

/**
 * （批量）删除 信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
    return request.detele(
        `${PATH}/${ids}`
    );
}

/**
 *  更新订单数据-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
    return request.put(
        `${PATH}`, data
    );
}

/**
 * 根据Id查询订单详情-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
    return request.get(
        `${PATH}/${id}`
    );
}

/**
 * 查询订单表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
    return request.get(
        `${PATH}`
    );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
    return request.get(
        `${PATH}/pages`, data
    );
}








