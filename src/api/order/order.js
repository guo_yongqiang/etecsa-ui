import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'order',
  MAPPING: 'orders'
};

const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 下单
 *
 * @param data
 * @returns {Promise}
 */
export function payOrder(data) {
  return request.post(`${PATH}`, data);
}
/**
 * 批量删除订单
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.detele(`${PATH}/${ids}`);
}

/**
 *  更新订单数据-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(`${PATH}`, data);
}

/**
 * 根据Id查询订单详情-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(`${PATH}/${id}`);
}

/**
 * 查询订单表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(`${PATH}`);
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.post(`${PATH}/pages`, data);
}

/**
 * 支付成功的回调
 *
 * @param orderId
 * @returns {Promise}
 */
export function paySuccess(orderId) {
  return request.post(`${PATH}/pay-success`, orderId);
}

/**
 * 批量发货/单个发货
 *
 * @param data
 * @returns {Promise}
 */
export function delivery(data) {
  return request.post(`${PATH}/delivery`, data);
}

/**
 * 确认收货
 *
 * @param orderId
 * @returns {Promise}
 */
export function receipt(orderId) {
  return request.post(`${PATH}/receipt`, orderId);
}

/**
 * 按状态查询订单
 *
 * @param status
 * @returns {Promise}
 */
export function conditionalInquiryOrder(status) {
  return request.get(`${PATH}/conditional-order/${status}`, status);
}
