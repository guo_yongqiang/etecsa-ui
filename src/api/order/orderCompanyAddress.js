import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'order',
  MAPPING: 'order-company-address'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 创建收发货地址
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}
/**
 * （批量）删除收发货地址
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.detele(
    `${PATH}/${ids}`
  );
}

/**
 * 更新收发货地址表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 根据Id查询收发货地址详情-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询收发货地址-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数查询收发货地址-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}

/**
 * 设置默认的地址-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function setDefaultAddress(id) {
  return request.put(
    `${PATH}/set-default`, id
  );
}


/**
 * 查询用户默认地址-API接口信息
 *
 * @param 
 * @returns {Promise}
 */
export function getDefaultAddress() {
  return request.get(
    `${PATH}/get-default`
  );
}

                                                                                    