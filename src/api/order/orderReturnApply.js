import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'order',
  MAPPING: 'order-return-applys'
};

const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 申请退货
 *
 * @param data
 * @returns {Promise}
 */
export function reqAbort(data) {
  return request.post(
    `${PATH}/reqAbort`, data
  );
}

/**
 * 描述：（批量）删除订单退货申请
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.detele(
    `${PATH}/${ids}`
  );
}

/**
 *  更新订单退货申请-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 根据Id查询订单退货申请-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询订单退货申请-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}

/**
 * 申请退货
 *
 * @param data
 * @returns {Promise}
 */
export function create(data) {
  return request.post(
    `${PATH}/create`, data
  );
}
