import request from '@/utils/request';

const DOC = {
  MODULE: 'paper',
  MAPPING: 'qms-papers'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 创建问卷调查
 * @param  data
 * @returns {Promise}
 */
export function creatPaper(data) {
  return request.post(`${PATH}/saveParper`, data);
}
