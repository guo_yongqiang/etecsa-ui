import request from '@/utils/request';

const DOC = {
  MODULE: 'production',
  MAPPING: 'product-brands'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 获取商品分类
 *
 * @returns {Promise}
 */
export function getBrand() {
  return request.get(`${PATH}`);
}
