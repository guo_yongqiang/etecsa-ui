import request from '@/utils/request';

const DOC = {
  MODULE: 'production',
  MAPPING: 'fileManager'
};

export const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

export const URL = {
  upload: `${PATH}/upload`,
  batchUpload: `${PATH}/batchupload`
};

/**
 * 创建分销商
 *
 * @returns {Promise}
 */
export function upload() {
  return request.post(URL.upload);
}

/**
 * 批量上传
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function batchUpload() {
  return request.post(URL.batchUpload);
}
