import request from '@/utils/request';

const DOC = {
  MODULE: 'production',
  MAPPING: 'product-category/tree'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 获取商品分类
 *
 * @returns {Promise}
 */
export function getCate() {
  return request.get(`${PATH}`);
}
