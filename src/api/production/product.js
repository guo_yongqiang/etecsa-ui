import request from '@/utils/request';

const DOC = {
  MODULE: 'production',
  MAPPING: 'product'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 创建商品
 * @param data
 * @returns {Promise}
 */
export function creatProduct(data) {
  return request.post(`${PATH}`, data);
}

/**
 * 更新商品
 * @param id
 * @param data
 * @returns {Promise}
 */
export function updateProduct(id, data) {
  return request.put(`${PATH}`, data);
}

/**
 * 获取商品详情信息
 * @param id
 * @returns {Promise}
 */
export function getProduct(id) {
  return request.get(`${PATH}/ProductDetail/${id}`);
}
/**
 * 获取商品信息
 * @param data
 * @returns {Promise}
 */
export function getProducts(data) {
  return request.post(`${PATH}/pages`, data);
}
