import request from '@/utils/request';

const DOC = {
  MODULE: 'doc',
  MAPPING: 'open-api-params'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * （批量）删除系统资源-API接口响应信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}
