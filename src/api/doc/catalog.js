import request from '@/utils/request';

const DOC = {
  /**
   * 和main.js保持一致，后期再改；否则请求报错
   */
  MODULE: 'doc'
};

/**
 * 创建目录表
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${DOC.MODULE}/catalogs`, data
  );
}

/**
 * （批量）删除目录表
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${DOC.MODULE}/catalogs/${ids}`
  );
}

/**
 * 更新分销商信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function updateById(id, data) {
  return request.put(
    `${DOC.MODULE}/catalogs/${id}`, data
  );
}

/**
 * 根据Id查询目录表
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${DOC.MODULE}/catalogs/${id}`
  );
}

/**
 * 根据Id查询某一目录表下的所有api
 *
 * @param id
 * @returns {Promise}
 */
export function getOpenApisById(id) {
  return request.get(
    `${DOC.MODULE}/catalogs/${id}/open-apis`
  );
}

/**
 * 根据父目录ID获取子目录集合
 *
 * @returns {Promise}
 */
export function getChildrenCatalogs(parentId) {
  return request.get(
    `${DOC.MODULE}/catalogs/${parentId}/sub-catalogs`
  );
}

/**
 * 获取目录树
 *
 * @returns {Promise}
 */
export function getCatalogsTree() {
  return request.get(
    `${DOC.MODULE}/catalogs/catalogs-tree`
  );
}

/**
 * 查询某一目录下的接口变化历史记录表列表信息
 *
 * @returns {Promise}
 */
export function listVoByCatalog(catalogId) {
  return request.get(
    `${DOC.MODULE}/catalogs/${catalogId}/open-api-change-histories`
  );
}
