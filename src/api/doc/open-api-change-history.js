import request from '@/utils/request';
import { param } from '@/utils';

const DOC = {
  MODULE: 'doc',
  MAPPING: 'open-api-change-histories'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 创建接口变化历史记录表信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 批量）删除接口变化历史记录表信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 更新接口变化历史记录表信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function updateById(id, data) {
  return request.put(
    `${PATH}/${id}`, data
  );
}

/**
 * 根据Id查询接口变化历史记录表详细信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询接口变化历史记录表列表信息
 *
 * @returns {Promise}
 */
export function list(json) {
  return request.get(
    `${PATH}?${param(json)}`
  );
}

/**
 * 描述：根据Id查询接口变化历史记录表详细信息
 *
 * @returns {Promise}
 */
export function page(json) {
  return request.get(
    `${PATH}/pages?pageNo=${json.pageNo}&pageSize=${json.pageSize}&${param(json.params)}`
  );
}
