import request from '@/utils/request';

const AUTH = {
  MODULE: 'authorization',
  MAPPING: 'oauth'
};

export const PATH = `${AUTH.MODULE}/${AUTH.MAPPING}`;

/**
 * 登录会员系统
 *
 * @param data
 * @return {Promise}
 */
export function loginMemberSystem(data) {
  const form = {
    username: data.username,
    password: data.password,
    grant_type: 'password',
    scope: 'all'
  };

  return request.post(`${PATH}/token`, null,
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic bWVtYmVyOnNlY3JldA=='
      },
      params: form
    }
  );
}

/**
 * 登录分销系统
 *
 * @param data
 * @return {Promise}
 */
export function loginDistributionSystem(data) {
  const form = {
    username: data.username,
    password: data.password,
    grant_type: 'dis',
    scope: 'all'
  };

  return request.post(`${PATH}/token`, null,
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ZGlzOnNlY3JldA=='
      },
      params: form
    }
  );
}

/**
 * 登录后台系统
 *
 * @param data
 * @return {Promise}
 */
export function loginSysUser(data) {
  const form = {
    username: data.username,
    password: data.password,
    grant_type: 'sys-user',
    scope: 'all'
  };

  return request.post(`${PATH}/token`, null,
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic c3lzLXVzZXI6c2VjcmV0'
      },
      params: form
    }
  );
}

/**
 * 获取当前登录人信息
 *
 * @return {Promise}
 */
export function getInfo() {
  return request.get(`${PATH}/current-info`);
}

/**
 * 注销
 *
 * @return {Promise}
 */
export function logout() {
  return request.get(`${AUTH.MODULE}/logout`);
}
