import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'production',
  MAPPING: '/product-home-categorys'
};

const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 创建商品首页分类
 *
 * @param data
 * @returns {Promise}
 */
export function postCategory(data) {
  return request.post(`${PATH}`, data);
}
/**
 * 更新商品首页分类
 *
 * @param data
 * @returns {Promise}
 */
export function putCategory(data) {
  return request.put(`${PATH}`, data);
}
/**
 * 获取商品首页分类
 *
 * @param data
 * @returns {Promise}
 */
export function getCategory(data) {
  return request.get(`${PATH}/pages`, data);
}
/**
 * 删除商品首页分类
 *
 * @param ids
 * @returns {Promise}
 */
export function removeCategory(ids) {
  return request.delete(`${PATH}/${ids}`);
}
/**
 * 获取商品首页分类详情
 *
 * @param ids
 * @returns {Promise}
 */
export function detailCategory(ids) {
  return request.get(`${PATH}/${ids}`);
}
