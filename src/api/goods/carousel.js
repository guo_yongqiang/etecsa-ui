import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'production',
  MAPPING: 'product-carousels'
};

const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 创建轮播图
 *
 * @param data
 * @returns {Promise}
 */
export function postCarousel(data) {
  return request.post(`${PATH}`, data);
}
/**
 * 删除轮播图
 *
 * @param ids
 * @returns {Promise}
 */
export function removeCarousel(ids) {
  return request.delete(`${PATH}/${ids}`);
}
/**
 * 更新轮播图
 *
 * @param data
 * @returns {Promise}
 */
export function putCarousel(data) {
  return request.put(`${PATH}`, data);
}
/**
 * 获取轮播图详情
 *
 * @param ids
 * @returns {Promise}
 */
export function getCarouselDetail(ids) {
  return request.get(`${PATH}/${ids}`);
}
/**
 * 获取轮播图
 *
 *
 * @returns {Promise}
 */
export function getCarousel() {
  return request.get(`${PATH}`);
}
