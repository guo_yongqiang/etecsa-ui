import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'ucenter',
  MAPPING: 'sys-users'
};

const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 分配角色-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function userSetRole(data) {
  return request.post(
    `${PATH}/userSetRole`, data
  );
}

/**
 * 创建后台用户-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function createUser(data) {
  return request.get(
    `${PATH}`, data
  );
}

/**
 * 批量删除-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function deleteById(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 更新-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 修改密码-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function updatePwd(data) {
  return request.put(
    `${PATH}/editpassword`, data
  );
}

/**
 * 根据Id查询后台用户表 -API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询后台用户表 -API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 查询后台用户表 -API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function page(data) {
  return request.post(
    `${PATH}/pages`, data
  );
}

/**
 * 增加用户权限用户表 -API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function addUserPermission(data) {
  return request.post(
    `${PATH}/add-permission`, data
  );
}

/**
 * 查询用户许可树-API接口信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function selectUserPermissionTree(id) {
  return request.get(
    `${PATH}/permission-tree/${id}`, id
  );
}

/**
 * 判断用户是否有访问uri的权限-API接口信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function queryUserRolePermission(username, uri) {
  return request.get(
    `${PATH}/role/permission`, username, uri
  );
}

/**
 * 查询用户的角色-API接口信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function queryUserRole(id) {
  return request.get(
    `${PATH}/role/${id}`, id
  );
}
