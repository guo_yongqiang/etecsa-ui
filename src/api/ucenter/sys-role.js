import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'ucenter',
  MAPPING: 'sys-roles'
};

const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 创建角色-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 查询该角色下所有用户-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function selectUsersByRole(roleId) {
  return request.get(
    `${PATH}/selectUsersByRole/${roleId}`
  );
}

/**
 * 查询该角色下所有权限-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function selectPersByRole(roleId) {
  return request.get(
    `${PATH}/selectPersByRole/${roleId}`
  );
}

/**
 * 批量删除-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function deleteById(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 更新角色表-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 修改角色权限-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateRolePermision(data) {
  return request.post(
    `${PATH}/updateRolePermision`, data
  );
}

/**
 * 修改角色权限-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询角色表 -API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 排除id的角色表列表信息 -API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function nexistsList(id) {
  return request.get(
    `${PATH}/nexists/${id}`
  );
}

/**
 * 查询后台用户表 -API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function page(data) {
  return request.post(
    `${PATH}/pages`, data
  );
}
