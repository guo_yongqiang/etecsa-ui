import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'pay',
  MAPPING: 'pay-banks'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 根据parentId查询 详细信息
 * @in
 * @returns {Promise}
 */
export function getByParentId(id) {
  return request.post(
    `${PATH}/parent/${id}`
  );
}


/**
 * 根据城市code码查询银行信息 详细信息-API接口信息
 *
 * @param code
 * @returns {Promise}
 */
export function getByCityCode(code) {
  return request.get(
    `${PATH}/${code}`
  );
}


/**
 * 创建银行信息-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 创建银行信息-API接口信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.detele(
    `${PATH}/${ids}`
  );
}

/**
 * 创建银行信息-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`,data
  );
}

/**
 * 查看银行列表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}





