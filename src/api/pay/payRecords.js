import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'pay',
  MAPPING: 'pay-records'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 新增交易记录 详细信息
 * 
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}/parent/${data}`
  );
}


/**
 * 批量删除交易记录-API接口信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.detele(
    `${PATH}/${ids}`
  );
}

/**
 * 更新交易记录-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 根据id查询交易记录-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.put(
    `${PATH}/${id}`, id
  );
}

/**
 * 查看交易记录列表-API接口信息
 *
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}





