import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'pay',
  MAPPING: 'pay-merchants'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 新增商户信息
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`,data
  );
}

/**
 * 新增商户信息
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.post(
    `${PATH}/${ids}`
  );
}

/**
 * 根据Id查询商户-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 根据参数查询商户信息-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function selectParams(data) {
  return request.post(
    `${PATH}/selectParams`,data
  );
}

/**
 * 查看银行列表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}





