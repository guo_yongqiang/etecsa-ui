import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'pay',
  MAPPING: 'pay-approval-callbacks'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 根据Id查询商户回调记录 详细信息
 *
 * @param data
 * @returns {Promise}
 */
export function selectParams(data) {
  return request.post(
    `${PATH}`, data
  );
}


/**
 * 根据Id查询商户回调记录 详细信息-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询订单表-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 根据参数分页查询-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}





