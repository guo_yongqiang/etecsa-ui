import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'product',
  MAPPING: 'product'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;
/**
 * 创建目录表
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 更新商品信息-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function updateProductById(data) {
  return request.put(
    `${PATH}/${id}`, data
  );
}

/**
 * 删除商品信息-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function deleteById(id) {
  return request.delete(
    `${PATH}/${id}`
  );
}

/**
 * 取消新品推荐-API接口信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function cancelById(id, data) {
  return request.put(
    `${PATH}/cancelrecommend/${id}`, data
  );
}

/**
 * 增减库存-API接口信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function changeProducttStock(id, data) {
  return request.put(
    `${PATH}/changeProducttStock/${id}`, data
  );
}

/**
 * 新品推荐下架
 *
 * @param id
 * @returns {Promise}
 */
export function downProductById(id) {
  return request.put(
    `${PATH}/downProduct/${id}`
  );
}

/**
 * 分页多条件查询商品信息-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.post(
    `${PATH}/pages/`, data
  );
}



/**
 * 获取商品详细信息
 *
 * @returns {Promise}
 */
export function getProductDetailById(id) {
  return request.get(
    `${PATH}/ProductDetail/${id}`
  );
}

/**
 * 根据param查询ProductVO
 *
 * @returns {Promise}
 */
export function queryProductDetailByParam() {
  return request.post(
    `${PATH}/ProductDetail/ProductParam`
  );
}


/**
 * 根据param查询ProductVO
 *
 * @returns {Promise}
 */
export function publishById(data) {
  return request.post(
    `${PATH}/publish`, data
  );
}

/**
* 根据分组id查询商品信息
*
* @returns {Promise}
*/
export function selectByGroupId(data) {
  return request.post(
    `${PATH}/selectByGroupId`, data
  );
}

