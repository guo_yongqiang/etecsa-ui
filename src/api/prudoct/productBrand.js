import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'product',
  MAPPING: 'product-brands'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 查询商品品牌列表信息
 *
 * @returns {Promise}
 */
export function getProductBrandList() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 新建品牌信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 更新 商品品牌信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateProductBrand(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 获取商品品牌详细信息
 *
 * @param id
 * @returns {Promise}
 */
export function getProductBrandById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 批量删除商品分类
 *
 * @param ids
 * @returns {Promise}
 */
export function deteleByIds(ids) {
  return request.datele(
    `${PATH}/${ids}`
  );
}

/**
 * 根据参数查询商品分页列表信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.post(
    `${PATH}/pages`, data
  );
}

