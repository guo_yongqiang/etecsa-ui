import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'product',
  MAPPING: 'product-skus'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;
/**
 * 创建sku的库存信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * （批量）删除sku的库存信息-API接口信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.put(
    `${PATH}/${ids}`
  );
}

/**
 * 更新sku的库存信息 -API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 根据Id查询sku的库存详细信息-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询sku的库存列表信息-API接口信息
 *
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`, 
  );
}

/**
 * 分页多条件查询商品信息-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages/`, data
  );
}

