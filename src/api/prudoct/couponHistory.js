import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'product',
  MAPPING: 'tscoupon-historys'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;
/**
 * 创建优惠券历史记录表
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * （批量）删除优惠券历史记录-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 更新优惠券历史-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateById(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 创建优惠券-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 查询优惠券历史记录-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 增减库存-API接口信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}



