import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'product',
  MAPPING: 'product-sku-stocks'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;
/**
 * 创建sku的库存信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 获取sku的库存信息集合
 *
 * @param id
 * @returns {Promise}
 */
export function findSkuBypId(id) {
  return request.get(
    `${PATH}/product/${id}`
  );
}

/**
 * 增减sku库存
 *
 * @param id
 * @returns {Promise}
 */
export function changeStock(id) {
  return request.put(
    `${PATH}/changeStock/${id}`
  );
}

/**
 * （批量）删除sku的库存信息 -API接口信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.detele(
    `${PATH}/${ids}`
  );
}

/**
 * 更新sku的库存信息-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function updateById(id, data) {
  return request.put(
    `${PATH}/${id}`, data
  );
}

/**
 * 获取sku的库存详细信息-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`,
  );
}

/**
 * 查询sku的库存列表信息-API接口信息
 *
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}/${id}`,
  );
}

/**
 * 分页多条件查询商品信息-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}

