import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'product',
  MAPPING: 'coupon-products'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;
/**
 * 查询优惠券列表信息
 *
 * @param data
 * @returns {Promise}
 */
export function getCouponList() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 获取优惠券详细信息-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getCouponById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 批量删除优惠券-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function deleteById(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 创建优惠券-API接口信息
 *
 * @param data
 * @returns {Promise}
 */
export function createCoupon(data) {
  return request.post(
    `${PATH}/create`, data
  );
}

/**
 * 增减库存-API接口信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}

/**
 * 新品推荐下架
 *
 * @param id
 * @returns {Promise}
 */
export function updateCouponById(id) {
  return request.post(
    `${PATH}/update`, id
  );
}



