import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'product',
  MAPPING: 'product-category'
};


const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 查询商品分类列表
 *
 * @returns {Promise}
 */
export function getProductCetegoryList() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 创建商品分类
 *
 * @param data
 * @returns {Promise}
 */
export function createProductCetegory(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 更新 商品分类信息
 *
 * @param data
 * @returns {Promise}
 */
export function updateProductCetegory(data) {
  return request.put(
    `${PATH}`, data
  );
}

/**
 * 获取商品分类信息详情
 *
 * @param id
 * @returns {Promise}
 */
export function getProductCetegoryById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 批量删除商品分类
 *
 * @param ids
 * @returns {Promise}
 */
export function deteleByIds(ids) {
  return request.datele(
    `${PATH}/${ids}`
  );
}

/**
 * 根据参数查询商品分页列表信息
 *
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.post(
    `${PATH}/pages`, data
  );
}

/**
 * 根据参数查询商品 分类分页列表信息
 * @param id
 * @param data
 * @returns {Promise}
 */
export function getCetegoryByParams(id, data) {
  return request.get(
    `${PATH}/pages/${id}`, data
  );
}

/**
 * 根据parentid查询所有分类信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function getProductCategoryByParentId(id, data) {
  return request.get(
    `${PATH}/parent/${id}`, data
  );
}

/**
 * 获取商品分类信息（树状结构）
 *
 * @param data
 * @returns {Promise}
 */
export function getTreeProductCategory(data) {
  return request.get(
    `${PATH}/tree`, data
  );
}


