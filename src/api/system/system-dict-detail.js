import request from '@/utils/request';
import { param } from '@/utils';

const DOC = {
  MODULE: 'system',
  MAPPING: 'system-dict-details'
};

const PATH = `${DOC.MODULE}/${DOC.MAPPING}`;

/**
 * 创建系统字典 信息
 *
 * @param data
 * @returns {Promise}
 */
export function save(data) {
  return request.post(
    `${PATH}`, data
  );
}

/**
 * 描述：（批量）删除系统字典 信息
 *
 * @param ids
 * @returns {Promise}
 */
export function removeByIds(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 描述：（批量）删除系统字典 信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function updateById(id, data) {
  return request.put(
    `${PATH}/${id}`, data
  );
}

/**
 * 描述：根据Id查询系统字典 详细信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}

/**
 * 描述：查询系统字典 列表信息
 *
 * @returns {Promise}
 */
export function list() {
  return request.get(
    `${PATH}`
  );
}

/**
 * 描述：查询系统字典 列表信息
 *
 * @returns {Promise}
 */
export function listByParentCode(parentCode) {
  return request.get(
    `${PATH}/parent-code/${parentCode}`
  );
}

/**
 * 描述：查询系统字典 列表信息
 *
 * @returns {Promise}
 */
export function page(parentCode, json) {
  return request.get(
    `${PATH}/parent-code/${parentCode}/pages?pageNo=${json.pageNo}&pageSize=${json.pageSize}&${param(json.params)}`
  );
}
