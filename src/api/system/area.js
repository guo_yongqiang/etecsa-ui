import request from '@/utils/request';

const PRODUCT = {
  MODULE: 'system',
  MAPPING: 'system-area'
};

const PATH = `${PRODUCT.MODULE}/${PRODUCT.MAPPING}`;

/**
 * 获取area详细信息-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function getById(id) {
  return request.get(
    `${PATH}/${id}`
  );
}
/**
 * 批量删除-API接口信息
 *
 * @param id
 * @returns {Promise}
 */
export function deleteById(ids) {
  return request.delete(
    `${PATH}/${ids}`
  );
}

/**
 * 获取树状结构地址数据表-API接口信息
 *
 * @returns {Promise}
 */
export function getAreaTree() {
  return request.get(
    `${PATH}/area/tree`
  );
}

/**
 * 增减-API接口信息
 *
 * @param id
 * @param data
 * @returns {Promise}
 */
export function pages(data) {
  return request.get(
    `${PATH}/pages`, data
  );
}
