const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {
  pwa: {
    iconPaths: {
      favicon32: 'favicon.ico',
      favicon16: 'favicon.ico',
      appleTouchIcon: 'favicon.ico',
      maskIcon: 'favicon.ico',
      msTileImage: 'favicon.ico'
    }
  },
  devServer: {
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      '/doc': {
        target: 'http://10.1.20.27:8765',
        changeOrigin: true,
        pathRewrite: {
          '^/doc': '' // 路径重写
        }
      },
      '/distribution': {
        target: 'http://10.1.20.27:8781',
        changeOrigin: true,
        pathRewrite: {
          '^/distribution': '' // 路径重写
        }
      },
      '/production': {
        target: 'http://10.1.20.14:8770',
        changeOrigin: true,
        pathRewrite: {
          '^/production': '' // 路径重写
        }
      },
      '/paper': {
        target: 'http://10.1.20.14:8084',
        changeOrigin: true,
        pathRewrite: {
          '^/paper': '' // 路径重写
        }
      },
      '/authorization': {
        target: 'http://10.1.20.27:8767',
        changeOrigin: true,
        pathRewrite: {
          '^/authorization': '' // 路径重写
        }
      },
      '/system': {
        target: 'http://10.1.20.27:8866',
        changeOrigin: true,
        pathRewrite: {
          '^/system': '' // 路径重写
        }
      },
      '/order': {
        target: 'http://10.1.20.27:8088',
        changeOrigin: true,
        pathRewrite: {
          '^/order': '' // 路径重写
        }
      }
    }
  },
  chainWebpack(config) {
    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end();
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end();
  }
};
